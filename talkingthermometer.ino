#include <Arduino.h>
#include <stdint.h>
#include "Talkie.h"
#include "Vocab_DE_Nums.h"

/*
 * global variables
 * change to fit your pin layout
 */
static const int8_t button1Pin = 2;
// 0°C = 273.15 K
static const double celsius0 = 273.15;
// temp1 is read on analog pin A3
static const int8_t temp1Pin = A3;
// 0 V corresponds to 0°C
static const double temp1_min = 0 + celsius0;
// 5 V corresponds to 100°C
static const double temp1_max = 100 + celsius0;
static Talkie voice; // uses pin 3 (and inverted pin 11) on Uno and Nano. As of now (2020-01-21) this is hardcoded.

// custom possibilty for digital input
static const int8_t UNCHANGED = -1;

void setup(void) {
	/*
	 * Talkie stuff
	 */
#if defined(CORE_TEENSY)
	pinMode(5, OUTPUT);
	digitalWrite(5, HIGH); //Enable Amplified PROP shield
#endif

	/*
	 * my pin usages
	 */
	pinMode(button1Pin, INPUT);
	voice.say(sp_NEUSTART);
}

void loop(void) {
	// button changed from LOW to HIGH
	if (checkButton1() == HIGH) {
		// first shut up
		if (voice.isTalking())
			voice.terminate();
		// read and say current temp
		sayNumber(celsius(readTemp1()), 1);
		voice.sayQ(sp_GRAD);
		voice.sayQ(sp_CELSIUS);
	}
}

static int8_t checkButton1(void) {
	static int8_t prevState;
	return checkDigRead(button1Pin, &prevState);
}

static double readTemp1(void) {
	return readMapAnalog(temp1Pin, temp1_min, temp1_max);
}

/*
 * convert from kelvin to celsius
 */
static double celsius(double kelvin) {
	return kelvin - celsius0;
}

/*
 * read an analog pin and map it linearly between a min and max value
 * int8_t analogPin: the pin to read
 * double minVal: 0 V gets mapped to this
 * double maxVal: 5 V gets mapped to this
 * Example: When there are 3 V at pin A3 then readMapAnalog(A3, 0, 10) returns 6.
 */
static double readMapAnalog(int8_t analogPin, double minVal, double maxVal) {
	// manually map, because map() uses int maths only
	return analogRead(analogPin) * (maxVal - minVal) / 1023 + minVal;
}

/*
 * get the state of a digital input only if it is different from the previous one
 * int8_t pin: digital input pin
 * int8_t *prevState: pointer to previous value
 * returns:	LOW/HIGH if digitalRead(pin) returns LOW/HIGH and *prevState is HIGH/LOW
 *		UNCHANGED if digitalRead(pin) is equal to *prevState
 * Example: int8_t checkState = checkDigRead(pin, &prevState);
 */
static int8_t checkDigRead(int8_t pin, int8_t* prevState) {
	int8_t curState = digitalRead(pin);
	if (*prevState != curState) {
		*prevState = curState;
		return curState;
	}
	return UNCHANGED;
}

static void sayDigit(int8_t d) {
	switch (d) {
		case 0:
			voice.sayQ(sp_NULL);
			break;
		case 1:
			voice.sayQ(sp_EINS);
			break;
		case 2:
			voice.sayQ(sp_ZWEI);
			break;
		case 3:
			voice.sayQ(sp_DREI);
			break;
		case 4:
			voice.sayQ(sp_VIER);
			break;
		case 5:
			voice.sayQ(sp_FUNF);
			break;
		case 6:
			voice.sayQ(sp_SECHS);
			break;
		case 7:
			voice.sayQ(sp_SIEBEN);
			break;
		case 8:
			voice.sayQ(sp_ACHT);
			break;
		case 9:
			voice.sayQ(sp_NEUN);
			break;
		default:
			break;
	}
}

/*
 * Say any number in the int-range between -999999 and 999999 (in German)
 * double n: The number to say 
 * uint8_t roundDigits: How many digits after the comma should be said (last one will get rounded)
 * Example: sayNumber(34.236, 2) will say "Vierunddreißig Komma Zwei Vier"
 * Note: function uses sayQ(), not say() --> thread goes on while voice is talking
 */
static void sayNumber(double n, uint8_t roundDigits) {
	// do the rounding at the correct digit
	n = round(n * pow(10, roundDigits)) / pow(10, roundDigits);
	// say the pre-decimal digits
	int32_t int_n = (int32_t)n;
	sayNumber(int_n);
				
	// say the decimals
	if (roundDigits) {
		n = abs(n - int_n);
		voice.sayQ(sp_KOMMA);
		for (int8_t digit = 1; digit <= roundDigits; ++digit) {
			double d_newDigit = n * pow(10, digit) - 10 * (int32_t)(n * pow(10, digit-1));
			int8_t newDigit;
			// round correctly for the last digit
			if (digit == roundDigits)
				newDigit = (int8_t)round(d_newDigit);
			else
				newDigit = (int8_t)d_newDigit;
			sayDigit(newDigit);
		}
	}
}

static void sayNumber(int32_t n) {
	// Negative numbers are spoken the same way as positive numbers only with "Minus" at the beginning.
	if (n < 0) {
		voice.sayQ(sp_MINUS);
		sayNumber(-n);
	// 1-digit numbers
	} else if (n < 10) {
		sayDigit(n);
	// 2-digit numbers
	} else if (n < 100) {
		// extra cases for 11 and 12
		switch (n) {
			case 11:
				voice.sayQ(sp_ELF);
				break;
			case 12:
				voice.sayQ(sp_ZWOLF);
				break;
			default:
				// German specific: Say the 1-digit first, then the 10-digit
				int32_t tens = n / 10;
				int32_t ones = n - 10 * tens;
				// first say the 1-digit
				if (ones) {
					sayPreNumber(ones);
					// UND only for numbers > 20. (VierUNDzwanzig, but not VierUNDzehn.)
					if (tens >= 2)
						voice.sayQ(sp_UND);
				}
				// then say the 10-digit
				switch (tens) {
					case 1:
						voice.sayQ(sp_ZEHN);
						break;
					case 2:
						voice.sayQ(sp_ZWANZIG);
						break;
					case 3:
						voice.sayQ(sp_DREISSIG);
						break;
					case 4:
						voice.sayQ(sp_VIERZIG);
						break;
					case 5:
						voice.sayQ(sp_FUNFZIG);
						break;
					case 6:
						voice.sayQ(sp_SECHZIG);
						break;
					case 7:
						voice.sayQ(sp_SIEBZIG);
						break;
					case 8:
						voice.sayQ(sp_ACHTZIG);
						break;
					case 9:
						voice.sayQ(sp_NEUNZIG);
						break;
					default:
						break;
				}
				break;
		}
	// 3-digit numbers
	} else if (n < 1000) {
		int32_t hundreds = n / 100;
		sayPreNumber(hundreds);
		voice.sayQ(sp_HUNDERT);
		sayAftNumber(n - 100 * hundreds);
	// 6-digit numbers
	} else {
		int32_t thousands = n / 1000;
		sayPreNumber(thousands);
		voice.sayQ(sp_TAUSEND);
		sayAftNumber(n - 1000 * thousands);
	}
}

/*
 * Same as sayNumber(int32_t n) only for 1 it says EIN instead of EINS.
 */
static void sayPreNumber(int32_t n) {
	if (n == 1)
		voice.sayQ(sp_EIN);
	else
		sayNumber(n);
}

/*
 * Same as sayNumber(int32_t n) only for 0 it does nothing
 */
static void sayAftNumber(int32_t n) {
	if (n)
		sayNumber(n);
}
