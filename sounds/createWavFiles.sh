#!/bin/sh

# This script creates the directory ./wav/ and puts audio files (.wav) of the spoken words in the wordlist (see below) there.
# espeak or espeak-ng need to be installed for this script to work

# You can list all voices for a language (in this case "de") with the command
# espeak --voices=de
# You can then try out the voices with
# espeak -v german-mbrola-3 "Test text."

voice=german-mbrola-5
speed=160

wordlist=(Null Eins Ein Zwei Drei Vier Fünf Sechs Sieben Acht Neun Zehn Elf Zwölf Zwanzig Dreißig Vierzig Fünfzig Sechzig Siebzig Achtzig Neunzig Hundert Tausend Komma Und Minus Grad Celsius Kelvin Fehler Höchstwert Niedrigstwert Neustart)


mkdir -p ./wav
for word in ${wordlist[*]}
do
	# for the filenames convert them to ascii-only characters
	asciiword="$(echo $word | iconv -f utf-8 -t ascii//translit)"
	espeak -v $voice -z -s $speed -w "./wav/$asciiword.wav" "$word"
done
