# talkingThermometer

The goal is to create a (German) talking thermometer with an Arduino Uno.

Dependencies for running this on your Arduino:
- [Talkie](https://github.com/ArminJo/Talkie)

Dependencies if you want to use your own words/language:
- [eSpeak NG](https://github.com/espeak-ng/espeak-ng) for converting text to speech (in the form of wav-files)
- [python_wizard](https://github.com/ptwz/python_wizard) to convert those wav-files to the byte-arrays the Arduino library [Talkie](https://github.com/ArminJo/Talkie) can handle

How to use your own words/language:
- Install [eSpeak NG](https://github.com/espeak-ng/espeak-ng) and [python_wizard](https://github.com/ptwz/python_wizard)
- Go into the directory `./sounds/`
- Edit the file `createWavFiles.sh` to include the words you want in the voice/language you want
- Run the file `./createWavFiles.sh`. It will create a directory `./wav/` with all your words as audio files.
- Edit the file `createVocabFromWav.py` and specify the path where you've installed [python_wizard](https://github.com/ptwz/python_wizard)
- Run the file `./createVocabFromWav.py` with arguments specifying the desired name of your word library and the wav-files. For example:
`./createVocabFromWav.py -n DE_Nums ./wav/`
- Now you should find the .c- and .h-file that you need for Talkie to say your desired words in your working directory
- Change the header of `talkingthermometer.ino` to include your .h-file
- Rewrite `talkingthermometer.ino` to use your word library instead of mine. A bit more advanced rewriting may need to be done to the function `sayNumber(double n, int roundDigits)` to satisfy the specific way numbers are spoken in your language.
